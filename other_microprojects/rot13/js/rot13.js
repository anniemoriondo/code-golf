function rot13(input){
  let alphabet = "abcdefghijklmnopqrstuvwxyz";
  let output = '';
  for(x = 0; x < input.length; x++){
    let letter = input[x];
    let lowercaseLetter = letter.toLowerCase();
    let letterIndex = alphabet.indexOf(lowercaseLetter);
    if(letterIndex >= 0 && letterIndex < 13){
      output += alphabet[letterIndex + 13];
    } else if (letterIndex >= 13){
      output += alphabet[letterIndex - 13];
    } else {
      output += letter;
    }

  }
  return output;
}

$('#translate').on('click', function(e){
  e.preventDefault;
  let inputText = $('#inputArea').val();
  let outputText = rot13(inputText);
  $('#outputArea').text(outputText);
});
