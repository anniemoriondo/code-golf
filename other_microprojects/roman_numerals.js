const numeral_values = {
  m: 1000,
  d: 500,
  c: 100,
  l: 50,
  x: 10,
  v: 5,
  i: 1,
}

function translate_roman(input_string){
  string_to_translate = input_string.toLowerCase();

  translated_digits = [];
  //this can be done with a map or somesuch.
  for (let x = 0; x < string_to_translate.length; x++){
    translated_digits.push(numeral_values[string_to_translate[x]]);
}

  for (let x = 0; x < translated_digits.length; x++){
    let thisDigit = translated_digits[x];
    let invertDigit = false;
    for (let y = x+1; y < translated_digits.length; y++){
      if (translated_digits[y] > thisDigit){
        invertDigit = true;
      }
    }
    if (invertDigit){
      translated_digits[x] = translated_digits[x] * -1;
    }
}

return translated_digits.reduce(function (a, b){
  return a + b;
});

}

console.log(translate_roman('MMXVII'));
console.log(translate_roman('IIX'));
console.log(translate_roman('XL'));
console.log(translate_roman('MCMXCII'));
